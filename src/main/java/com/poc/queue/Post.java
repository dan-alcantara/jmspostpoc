package com.poc.queue;

import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;

import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

public class Post {

	private final static String JNDI_FACTORY = "weblogic.jndi.WLInitialContextFactory";
	private final static String JMS_FACTORY = "jms/ExtracaoExecucaoIntegracaoFactory";
	private final static String QUEUE_NAME = "jms/ExtracaoExecucaoIntegracaoQueue";
	private final static String IP = "t3://localhost:7001";

	public static void main(String[] args) throws Exception {

		QueueConnectionFactory qconFactory;
		QueueConnection qcon;
		QueueSession qsession;
		QueueSender qsender;
		Queue queue;
		TextMessage message;

		String ip = IP;

		if (args != null && args.length > 0) {
			ip = args[0];
		}

		Hashtable env = new Hashtable();
		env.put(Context.INITIAL_CONTEXT_FACTORY, JNDI_FACTORY);
		env.put(Context.PROVIDER_URL, ip);
		Context context = new InitialContext(env);

		qconFactory = (QueueConnectionFactory) context.lookup(JMS_FACTORY);
		qcon = qconFactory.createQueueConnection();
		qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
		queue = (javax.jms.Queue) context.lookup(QUEUE_NAME);
		qsender = qsession.createSender(queue);
		message = qsession.createTextMessage();
		qcon.start();

		final Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			int n = 0;

			@Override
			public void run() {
				
				try {
					message.setText("Mensagem de teste " + (n + 1));
					System.out.println(message.toString());
					qsender.send(message);
				} catch (JMSException e) {
					e.printStackTrace();
				}
				
				if (++n == 5) {
					timer.cancel();
				}
			}
		}, 5000, 5000);

	}
}
